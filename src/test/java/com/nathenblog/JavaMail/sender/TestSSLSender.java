package com.nathenblog.JavaMail.sender;

import com.nathenblog.JavaMail.MailSenderInfo;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import java.io.File;

//注意好像qq 不能接收txt 格式 的附件 ， 其他格式的可以，不知问题出在哪
public class TestSSLSender {
    public static void testGMailSender() {
        // 这个类主要是设置邮件
        MailSenderInfo mailInfo = new MailSenderInfo();
        mailInfo.setMailServerHost("smtp.gmail.com");
        mailInfo.setMailServerPort("465");
        // mailInfo.setValidate(true);
        mailInfo.setUserName("jixiuf");
        mailInfo.setPassword("passwd");// 您的邮箱密码
        mailInfo.setFromAddress("jixiuf@gmail.com");
        mailInfo.setToAddress("jixiuf@qq.com");
        mailInfo.setSubject("设置邮箱标题 如http://www.guihua.org 中国桂花网");
        mailInfo.setContent("设置邮箱内容 如http://www.guihua.org 中国桂花网 是中国最大桂花网站==");
        File f1 = new File("d:\\text.txt");
        File f2 = new File("d:\\text.rar");
        File f3 = new File("d:\\1.jpg");
        mailInfo.setAttachFileNames(new File[]{f1, f2, f3});

        // 注意这里用的是有SSL验证的Sender
        MailSender sender = new SSLMailSender();
        try {// 发送两次，一次以html格式（此时附件会被发送），一次文本
            // sender.sendTextMail(mailInfo);
            sender.sendHtmlMail(mailInfo);
            System.out.println("邮件已发送");
        } catch (AddressException e) {
            System.err.println("发送失败");
            e.printStackTrace();
        } catch (MessagingException e) {
            System.err.println("发送失败");
            e.printStackTrace();
        }
    }

    public static void testQQSender() {
        // 这个类主要是设置邮件
        MailSenderInfo mailInfo = new MailSenderInfo();
        mailInfo.setMailServerHost("smtp.qq.com");
        mailInfo.setMailServerPort("465");
        // mailInfo.setValidate(true);
        mailInfo.setUserName("userName");//如果不是以@qq.com结尾，应写全称
        mailInfo.setPassword("password");// 您的邮箱密码
        mailInfo.setFromAddress("jixiuf@qq.com");
        mailInfo.setToAddress("jixiuf@qq.com");
        mailInfo.setSubject("qq javamail 中车465 ssl ");
        mailInfo.setContent("qq javamail中文 465 ssl ,just a test");
        File f1 = new File("d:\\text.txt");
        mailInfo.setAttachFileNames(new File[]{f1});
        // 注意这里用的是有SSL验证的Sender
        MailSender sender = new SSLMailSender();
        try {
            // 发送两次，一次以html格式（此时附件会被发送），一次文本
            // sender.sendTextMail(mailInfo);
            sender.sendHtmlMail(mailInfo);//html必须有file
            System.out.println("qq邮件已发送");
        } catch (AddressException e) {
            System.err.println("qq发送失败");
            e.printStackTrace();
        } catch (MessagingException e) {
            System.err.println("qq发送失败");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        // TestSSLSender.testQQSender();
        TestSSLSender.testGMailSender();
    }

}

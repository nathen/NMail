package com.nathenblog.JavaMail.sender;


import com.nathenblog.JavaMail.MailSenderInfo;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;


public interface MailSender {
    public void sendTextMail(MailSenderInfo mailInfo) throws AddressException,
            MessagingException;

    public void sendHtmlMail(MailSenderInfo mailInfo) throws AddressException,
            MessagingException;

}
